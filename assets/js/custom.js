// document.addEventListener('DOMContentLoaded', function () {
//     var toggleButton = document.querySelector('.toggle_button');
//     var closeButton = document.querySelector('.close');
//     var sidebar = document.querySelector('.sidebar');

//     // Toggle sidebar visibility
//     toggleButton.addEventListener('click', function () {
//         sidebar.classList.toggle('collapse');
//     });

//     // Close sidebar
//     closeButton.addEventListener('click', function () {
//         sidebar.classList.remove('collapse');
//     });
// });


document.addEventListener('DOMContentLoaded', function() {
    const slideContainers = document.querySelectorAll('.slide-container');

    slideContainers.forEach(container => {
        const slidesContainer = container.querySelector('.slides');
        const slides = container.querySelectorAll('.slide');
        const prevButton = container.querySelector('.nav-button.prev');
        const nextButton = container.querySelector('.nav-button.next');
        const slideWidth = slides[0].offsetWidth + 10; // Độ rộng của mỗi slide cộng với margin
        const visibleSlides = 10; // Số slide hiển thị trên màn hình
        const totalSlides = slides.length;
        let currentIndex = 0;

        // Ẩn nút prev và next nếu số slide nhỏ hơn số slide setting
        // if (totalSlides <= visibleSlides) {
        //     prevButton.style.display = 'none';
        //     nextButton.style.display = 'none';
        // }

        nextButton.addEventListener('click', function() {
            if (currentIndex < totalSlides - visibleSlides) {
                currentIndex++;
            } else {
                currentIndex = 0; // Quay về slide 1
            }
            slidesContainer.style.transform = `translateX(-${currentIndex * slideWidth}px)`;
        });

        prevButton.addEventListener('click', function() {
            if (currentIndex > 0) {
                currentIndex--;
            } else {
                currentIndex = totalSlides - visibleSlides; // Quay về slide cuối cùng
            }
            slidesContainer.style.transform = `translateX(-${currentIndex * slideWidth}px)`;
        });
    });
});


document.addEventListener('DOMContentLoaded', function() {
    var backToTopButton = document.getElementById('backToTop');

    window.addEventListener('scroll', function() {
        if (window.scrollY > 300) { // Hiển thị nút khi cuộn xuống hơn 300px
            backToTopButton.style.display = 'flex';
        } else {
            backToTopButton.style.display = 'none';
        }
    });

    backToTopButton.addEventListener('click', function() {
        window.scrollTo({ top: 0, behavior: 'smooth' });
    });
});